'use strict'

const tstId = 'treestyletab@piro.sakura.ne.jp'
const mthId = 'multipletab@piro.sakura.ne.jp'

var lastActiveTabId = -1
var lastActiveTabTime = 0
var hasMTHSelection = false

async function registerToTST () {
  const self = await browser.management.getSelf()
  await browser.runtime.sendMessage(tstId, {
    type: 'register-self',
    name: self.id
  })
}
registerToTST()

browser.runtime.onMessageExternal.addListener((aMessage, aSender) => {
  if (aSender.id === tstId) {
    switch (aMessage.type) {
      case ('tab-mousedown'):
        onMouseDown(aMessage)
        break

      case ('tab-mouseup'):
        onMouseUp(aMessage)
        break

      case ('ready'):
        registerToTST()
        break
    }
  }
})

browser.commands.onCommand.addListener(async command => {
  if (command === 'toggle-feature') {
    let activeWindowTabs = await browser.tabs.query({'currentWindow': true})
    activatePreviousTab(activeWindowTabs)
  }
})

function onMouseDown (event) {
  if (event.tab.active === true) {
    lastActiveTabId = event.tab.id
    lastActiveTabTime = Date.now()
  } else {
    lastActiveTabId = -1
  }
}

async function onMouseUp (event) {
  if (hasMTHSelection === true) {
    hasMTHSelection = false
    return
  }
  updatehasMTHselection()
  if (event.tab.id !== lastActiveTabId ||
      // if Multiple Tab Handler menu appear
      (Date.now() - lastActiveTabTime) > 500 ||
      event.button !== 0 ||
      event.ctrlKey === true ||
      event.shiftKey === true ||
      event.metaKey === true) {
    return
  }

  let windowTabs = await browser.tabs.query({'currentWindow': true})
  if (windowTabs.length >= 2 &&
      windowTabs.some(elem => elem.id === event.tab.id && elem.active === true)) {
    activatePreviousTab(windowTabs)
  }
}

async function updatehasMTHselection () {
  try {
    let selection = (await browser.runtime.sendMessage(mthId, {
      type: 'get-tab-selection'
    })).selected
    hasMTHSelection = selection.length > 0
  // if Multiple Tab Handler is not installed
  } catch (error) {}
}

function activatePreviousTab (tabs) {
  tabs.sort((a, b) => b.lastAccessed - a.lastAccessed)
  browser.tabs.update(tabs[1].id, {'active': true})
}
